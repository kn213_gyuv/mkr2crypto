﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

internal class Program
{
    static List<int> ComputePublicKey(List<int> superIncreasingSequence, int m, int n)
    {
        return superIncreasingSequence.Select(w => (n * w) % m).ToList();
    }

    static List<int> Encrypt(string plaintext, List<int> publicKey)
    {
        List<int> ciphertext = new List<int>();
        string[] chunks = plaintext.Split();

        foreach (string chunk in chunks)
        {
            int chunkValue = 0;
            for (int i = 0; i < chunk.Length; i++)
            {
                chunkValue += (chunk[i] - '0') * publicKey[i];
            }
            ciphertext.Add(chunkValue);
        }

        return ciphertext;
    }
    private static void Main(string[] args)
    {
        List<int> superIncreasingSequence = new List<int>
        { 5, 10, 17, 35 };
        int m = 63;
        int n = 37;
        string plaintext = "1001 1011 0100";

        // Обчислення публічного ключа
        List<int> publicKey = ComputePublicKey(superIncreasingSequence, m, n);
        Console.WriteLine("Public key: " + string.Join(", ", publicKey));

        // Шифрування тексту
        List<int> ciphertext = Encrypt(plaintext, publicKey);
        Console.WriteLine("Ciphertext: " + string.Join(", ", ciphertext));
    }
}