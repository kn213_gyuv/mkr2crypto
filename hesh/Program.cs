﻿internal class Program
{
    static int ModPow(int a, int b, int m)
    {
        if (b == 0)
            return 1;
        long t = ModPow(a, b / 2, m);
        long result = (t * t) % m;
        if (b % 2 == 1)
            result = (result * a) % m;
        return (int)result;
    }
    private static void Main(string[] args)
    {
        int h_M = 6;
        int r = 17;
        int s = 3;
        int p = 23;
        int g = 5;
        int y = 10;

        // Обчислення за формулою: g^h(M) % p
        int leftSide = ModPow(g, h_M, p);

        // Обчислення за формулою: y^r % p
        int yrModP = ModPow(y, r, p);

        // Обчислення за формулою: r^s % p
        int rsModP = ModPow(r, s, p);

        // Обчислення за формулою: (y^r * r^s) % p
        int rightSide = (yrModP * rsModP) % p;

        // Перевірка результатів на рівність
        bool isValid = leftSide == rightSide;

        Console.WriteLine($"Ліва сторона: {leftSide}\nПрава сторона: {rightSide}\nРезультат перевірки: {isValid}");
    }
}