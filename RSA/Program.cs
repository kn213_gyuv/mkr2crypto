﻿using System.Numerics;

BigInteger C = 525;
BigInteger d = 275;
BigInteger n = 19 * 29;

BigInteger M = BigInteger.ModPow(C, d, n);

Console.WriteLine("Decoded message (M): " + M);
