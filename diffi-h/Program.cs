﻿internal class Program
{
    public static int CalculateSharedSecret(int p, int g, int x, int y)
    {
        // Обчислення публічного значення A
        int A = ModPow(g, x, p);
        // Обчислення публічного значення B
        int B = ModPow(g, y, p);
        // Обчислення спільного секрету
        int sharedSecretA = ModPow(B, x, p);
        int sharedSecretB = ModPow(A, y, p);
        // Задання рівності
        if (sharedSecretA != sharedSecretB)
        {
            throw new Exception("Shared secrets do not match.");
        }
        return sharedSecretA;
    }

    // Функція для обчислення (a^b) % mod
    public static int ModPow(int a, int b, int mod)
    {
        if (b == 0)
            return 1;
        long t = ModPow(a, b / 2, mod);
        t = (t * t) % mod;
        if (b % 2 == 1)
            t = (t * a) % mod;
        return (int)t;
    }
    private static void Main(string[] args)
    {
        // Вхідні дані
        int p = 131;
        int g = 14;
        int x = 10;
        int y =15;
        // Обчислення спільного секретного ключа
        int sharedSecret = CalculateSharedSecret(p, g, x, y);
        Console.WriteLine("Shared secret key: " + sharedSecret);
    }
}