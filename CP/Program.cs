﻿using System.Numerics;

internal class Program
{
    public static BigInteger ModInverse(BigInteger a, BigInteger m)
    {
        BigInteger m0 = m, t, q;
        BigInteger x0 = 0, x1 = 1;

        if (m == 1)
            return 0;

        while (a > 1)
        {
            q = a / m;
            t = m;
            m = a % m;
            a = t;
            t = x0;
            x0 = x1 - q * x0;
            x1 = t;
        }

        if (x1< 0)
            x1 += m0;

        return x1;
    }
    private static void Main(string[] args)
    {
        int p = 29;
        int q = 13;

        BigInteger n = p * q;
        BigInteger phi_n = (p - 1) * (q - 1);

        int e = 13;
        if (BigInteger.GreatestCommonDivisor(e, phi_n) != 1)
        {
            throw new ArgumentException();
        }

        BigInteger d = ModInverse(e, phi_n);

        BigInteger M = 4;
        BigInteger C = BigInteger.ModPow(M, e, n);
        BigInteger decrypted_M = BigInteger.ModPow(C, d, n);

        Console.WriteLine($"Публічний ключ: ({e}, {n})");
        Console.WriteLine($"Приватний ключ: ({d}, {n})");
        Console.WriteLine($"Plaintext: {M}");
        Console.WriteLine($"Шифротекст: {C}");
        Console.WriteLine($"Дешифрований текст: {decrypted_M}");
    }
}